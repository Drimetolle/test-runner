using Dapper;
using DotNet.Testcontainers.Builders;
using DotNet.Testcontainers.Containers;
using Npgsql;


namespace TestProject1;

public class Tests
{
    private IContainer _container;

    [SetUp]
    public async Task SetUp()
    {
        _container = new ContainerBuilder()
            .WithImage("postgres")
            .WithPortBinding(5432)
            .WithEnvironment("POSTGRES_USER", "username")
            .WithEnvironment("POSTGRES_PASSWORD", "password")
            .WithEnvironment("POSTGRES_DB", "test")
            .WithEnvironment("POSTGRES_HOST_AUTH_METHOD", "trust")
            .WithWaitStrategy(Wait.ForUnixContainer().UntilPortIsAvailable(5432))
            .Build();

        await _container.StartAsync();
    }
    
    [Test]
    public async Task Test()
    {
        var connection = new NpgsqlConnection($"Server={_container.Hostname};Database=test;User Id=username;Password=password;");
        var result = await connection.QueryFirstAsync<string>("SELECT lower('Hello');");

        Assert.AreEqual("hello", result);
    }
}